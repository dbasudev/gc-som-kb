module.exports = {
    isEmptyObject(obj) {
        if (!obj) {
            return true;
        }

        return (obj == null || obj == undefined || Object.keys(obj).length == 0);
    },

    isNoMatch(jovo, app) {
        const intentName = jovo.$request.getIntentName();
        if (intentName == 'Default Welcome Intent') {
            console.log("isNoMatch for Welcome is false");
            return false;
        }
        if (jovo.$request?.queryResult?.action == 'input.unknown' && !this.isNoInput(jovo, app)) {
            console.log("isNoMatch? true");
            return true;
        }
        console.log("isNoMatch? false");
        return false;
    },

    isNoInput(jovo, app) {
        //see if request contained no_input context
        if (jovo.$request?.queryResult?.outputContexts.length > 0) {
            const outputContexts = jovo.$request?.queryResult?.outputContexts;
            for (let index = 0; index < outputContexts.length; index++) {
                if (
                    outputContexts[index].name.toLowerCase().includes("no_input")
                ) {
                    console.log("isNoInput? true");
                    return true;
                }
            }
        }
        console.log("isNoInput? false");
        return false;
    },

    // isIntentInCurrentState(jovo, app, stateName) {
    //     let intentName = jovo.$request.getIntentName();
    //     let currentStateVar = this.getStateName(stateName);
    //     let currentStateName = this.getIntentName(stateName);
    //     console.log('intents in state ' + stateName);
    //     let intents = this.getIntentsInState(jovo, app, stateName);
    //     console.log("{{\n" + intents + "\n}}");
    //     return intents && intents.includes(intentName);
    // },
    // isFunction(functionToCheck) {
    //     return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
    // },

    // /**
    //  * 
    //  * @param {*} jovo 
    //  * @param {*} app 
    //  * @returns the names of all DialogFlow intents in the current Jovo state
    //  */
    // getIntentsInState(jovo, app, stateName) {
    //     let currentStateVar = this.getStateName(stateName);
    //     let currentStateName = this.getIntentName(stateName);
    //     //console.log("In getIntentsInCurrentState; state = " + jovo.getState());
    //     //console.log("In getIntentsInCurrentState; currentStateVar = '" + currentStateVar + "'");
    //     //console.log("In getIntentsInCurrentState; currentStateName = '" + currentStateName + "'");
    //     let stateIntent = {};
    //     if (currentStateVar != "") {
    //         stateIntent = "kbState";//app.config.handlers[currentStateVar][currentStateName];
    //     } else {
    //         stateIntent = "kbState"; //app.config.handlers[currentStateName];
    //     }
    //     if (this.isFunction(stateIntent)) {
    //         return [currentStateName];
    //     }
    //     return this.findAllIntents(stateIntent);
    // },

    // findAllIntents(states) {
    //     console.log(states);
    //     let intents = [];
    //     for (const [key, value] of Object.entries(states)) {
    //         //console.log(`${key}: ${value}`);
    //         if (this.isFunction(value)) {
    //             intents.push(key);
    //         } else {
    //             intents = intents.concat(this.findAllIntents(value));
    //         }
    //     }
    //     return intents;
    // },
    // getStateName(stateNameIntentName) {
    //     if (stateNameIntentName?.includes('.')) {
    //         let state = stateNameIntentName.split('.')[0];
    //         return state;
    //     } else {
    //         return "";
    //     }
    // },
    // getIntentName(stateNameIntentName) {
    //     if (stateNameIntentName?.includes('.')) {
    //         let intent = stateNameIntentName.split('.')[1];
    //         return intent;
    //     } else {
    //         return stateNameIntentName;
    //     }
    // },
    defaultNoMatch(jovo) {
        let retriesCount = jovo.$session.$data.$retries || 0;
        console.log("State and intent from Jovo:" + jovo.getState());
        console.log(this.$reprompt);
        console.log("******************************* Repeat Reprompt: ", jovo.$session.$data.repeatReprompt);
        let initialPrompt = jovo.$session.$data.repeatReprompt || jovo.$session.$data.$lastSpeech;
        console.log("Initial prompt: " + initialPrompt);
        console.log("session: ");
        console.log(jovo.$session);
        if (retriesCount == 0) {
            console.log("**** In defaultNoMatch: first No-match");
            let speech = jovo.$speech;
            speech.addText(jovo.t("NM_1"));
            speech.addText(initialPrompt);
            jovo.followUpState(jovo.$session.$data.$state);
            jovo.ask(speech);

        } else if (retriesCount == 1) {
            console.log("**** In defaultNoMatch: second No-match");
            let speech = jovo.$speech;
            speech.addText(jovo.t("NM_2"));
            speech.addText(initialPrompt);
            jovo.followUpState(jovo.$session.$data.$state);
            jovo.ask(speech);

        } else {
            console.log("**** In defaultNoMatch: Max No-match");
            jovo.$speech.addText(jovo.t("250_M_1"));
            jovo.$session.$data.transferReason = "Max-NM";
            jovo.toStateIntent("", "ChatWrapup");
        }
    },

    defaultNoInput(jovo) {
        let noinputsCount = jovo.$session.$data.$noinputs || 0;
        console.log("\n**** In defaultNoInput: state and intent from Jovo:" + jovo.getState());
        console.log("**** In defaultNoInput: state to return to:" + jovo.$session.$data.$state);
        let initialPrompt = jovo.$session.$data.repeatReprompt || jovo.$session.$data.$lastSpeech;
        console.log("**** In defaultNoInput: Initial prompt to reprompt with: " + initialPrompt);
        //console.log("session: " );
        //console.log(jovo.$session);
        if (noinputsCount == 0) {
            console.log("**** In defaultNoInput: first noinput");
            let speech = jovo.$speech;
            speech.addText(jovo.t("NI_1"));
            speech.addText(initialPrompt);
            //console.log("**** In defaultNoInput: first noinput; played the reprompt, followup state is " + jovo.$session.$data.$state);
            jovo.followUpState(jovo.$session.$data.$state);
            jovo.ask(speech);

        } else if (noinputsCount == 1) {
            let speech = jovo.$speech;
            console.log("**** In defaultNoInput: second noinput");
            speech.addText(jovo.t("NI_2"));
            speech.addText(initialPrompt);
            //console.log("**** In defaultNoInput: second noinput; played the reprompt, followup state is " + jovo.$session.$data.$state);
            jovo.followUpState(jovo.$session.$data.$state);
            jovo.ask(speech);

        } else {
            console.log("**** In defaultNoInput: MaxNoInputs");
            jovo.$session.$data.disconnect = true;
            jovo.toStateIntent("", "ChatWrapup");
        }
    },
}