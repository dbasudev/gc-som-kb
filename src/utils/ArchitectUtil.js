module.exports = {
    getParameters(rawRequest) {
        if (rawRequest.queryResult) {
          return rawRequest?.queryResult?.outputContexts?.[0]?.parameters;
        } else {
          return null;
        }
      },
    
      getArchitectContext(rawRequest) {
        //console.log("************** getDesignerContext:: request:")
        //console.log(JSON.stringify(rawRequest, null, 4));
        let designerContext = {};
          if (rawRequest?.queryResult?.outputContexts.length > 0) {
            const outputContexts = rawRequest?.queryResult?.outputContexts;
            
            for (let index = 0; index < outputContexts.length; index++) {
              if (
                outputContexts[index].name.toLowerCase().includes("designercontext")
              ) {
                designerContext = outputContexts[index].parameters;
                //console.log("getDesignerContext: Found designer context: " + designerContext);
              }
            }
          }
          return designerContext;
      },
    
      //Gets all context parameter
      getAllParameters(rawRequest) {
        const contexts = rawRequest?.queryResult?.outputContexts;
        let ret = {};
        if (rawRequest.queryResult) {
          for (let index = 0; index < contexts.length; index++) {
            const parameters = contexts[index].parameters;
            Object.keys(parameters).forEach((key) => {
              ret[key] = parameters[key];
            });
          }
          return ret;
        } else {
          return null;
        }
      },
    
      //Gets the values of a given parameter. Optional default value if param not found
      getParameter(rawRequest, name, defaultVal = "Param not found") {
        const params = this.getParameters(rawRequest);
        if (params && params.hasOwnProperty(name)) {
          return params[name];
        }
        return defaultVal;
      }
}