const kbData = require("../KBData/kb.json");
module.exports = {
    getAnswers(query){
        let answer = null;
        for(let i = 0; i <= kbData.length; i++){
            if(kbData[i] === query){
                answer = kbData[i].Answer;
            }
        }
        return answer;
    }
}