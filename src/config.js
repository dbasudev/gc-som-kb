// ------------------------------------------------------------------
// APP CONFIGURATION
// ------------------------------------------------------------------

module.exports = {
  logging: false,

  intentMap: {
    "AMAZON.StopIntent": "END",
  },
  i18n: {
    returnNull: false,
    fallbackLng: "en",
  },
  user: {
    context: false,
  },

  db: {
    FileDb: {
      pathToFile: '../db/db.json',
    },
  },
};
