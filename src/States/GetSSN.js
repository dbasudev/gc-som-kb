module.exports = {
    get_ssn_start(){
        this.ask("You will need your DCN or Social Security number to get information about your case. Please type in your 10-digit DCN or 9-digit Social Security Number.");
        this.followUpState("getSSNState.getSSN");
    },
    
    getSSN: {
        Collect_SSN_DCN_UserInput() {
            let ssn = this.$inputs?.ssn_dcn?.key;
            console.log("You have entered", ssn);
            this.tell(`The social security number you entered is ${ssn}`);
        }
    }
}