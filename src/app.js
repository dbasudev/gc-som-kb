'use strict';

const { App, HttpService } = require('jovo-framework');
const { Alexa } = require('jovo-platform-alexa');
const { GoogleAssistant } = require('jovo-platform-googleassistant');
const { Dialogflow } = require("jovo-platform-dialogflow");
const { JovoDebugger } = require('jovo-plugin-debugger');
const { FileDb } = require('jovo-db-filedb');
const architect = require("./utils/ArchitectUtil");
const getSSNState = require("./States/GetSSN");
const Utils = require("./utils/Utils");


const app = new App();

app.use(
  new Dialogflow(),
  new Alexa(),
  new GoogleAssistant(),
  new JovoDebugger(),
  new FileDb()
);

//Sends context to designer using the sendToArchitectEvent
app.hook("response", async (error, host, jovo) => {
  //Check if there's data to send to architect


  console.log("\n\n app.hook: **********************************");
  console.log("Request: ");
  console.log("**********************************");
  //console.log(JSON.stringify(jovo?.$request, null, 3));
  console.log("**********************************");
  /** 
  console.log("**********************************");
  console.log("Response: ");
  console.log("**********************************");
  console.log(JSON.stringify(jovo?.$response, null, 3));
  console.log("**********************************\n\n");
  */
  // console.log("In app.hook: State from jovo hook: " + this.getState());
  // console.log("In app.hook: Intent from jovo hook: " + this.getIntentName());


  if (!Utils.isNoInput(jovo, app) && !Utils.isNoMatch(jovo, app)) {
    console.log("+++++++++++++++++++++++++++++++++++++ App Start +++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    console.log(jovo?.$output);
    console.log(jovo?.$output?.ask);
    console.log("+++++++++++++++++++++++++++++++++++++++ App End +++++++++++++++++++++++++++++++++++++++++++++++++++++");
    console.log("In app.hook: NOT NOMATCH and NOT NOINPUT: saving prompt for noinput / nomatch: ");
    //console.log(jovo?.$output?.ask);
    jovo.$session.$data.$retries = 0;
    jovo.$session.$data.$noinputs = 0;
    jovo.$session.$data.$lastSpeech = jovo.$session.$data.$reprompt ? jovo.$session.$data.$reprompt : (jovo?.$output?.ask?.speech || "");
    jovo.$session.$data.$reprompt = null;
    //this.$session.$data.mainMenuFlag = this.$session.$data.mainMenuFlag || false;
    jovo.$session.$data.$state = jovo.getState();
    console.log("In app.hook: Saving state to go back to for noinput / nomatch: " + jovo.$session.$data.$state);
  } else if (Utils.isNoMatch(jovo, app)) {
    jovo.$session.$data.$retries = jovo.$session.$data.$retries + 1;
    console.log("In app.hook: NOMATCH found; incremented $retries to: " + jovo.$session.$data.$retries);
  } else if (Utils.isNoInput(jovo, app)) {
    jovo.$session.$data.$noinputs = jovo.$session.$data.$noinputs + 1;
    console.log("In app.hook: NOINPUT found; incremented $noinputs to: " + jovo.$session.$data.$noinputs);
  }
  if (jovo.$session.$data.sendToArchitect) {
    console.log("Trigger sendToArchitect");
    let architectContextName = "";

    //find archContext Name
    if (jovo.$request?.queryResult?.outputContexts.length > 0) {
      const outputContexts = jovo.$request?.queryResult?.outputContexts;
      for (let index = 0; index < outputContexts.length; index++) {
        if (
          outputContexts[index].name.toLowerCase().includes("archcontext")
        ) {
          architectContextName = outputContexts[index].name;
        }
      }
    }
    //Check designerContextName
    console.log("architectContextName: " + architectContextName);

    //Save and override the DialogFlow Context with the data sent in the intent
    jovo.$response.outputContexts = [
      {
        name: architectContextName,
        lifespanCount: 1,
        parameters: jovo.$session.$data.sendToArchitect,
      },
    ];

    console.log("\n\narchContext sent to Architect:");
    console.log(JSON.stringify(jovo.$response.outputContexts[0], null, 3));
    console.log("++++++++++++++++++++++++++++++++++++++++++++++\n\n");

    //Set the response to trigger the sendToDesignerEvent
    jovo.$response.followupEventInput = {
      name: "SendToArchitectEvent",
    };
  }


});

// ------------------------------------------------------------------
// APP LOGIC
// ------------------------------------------------------------------

app.setHandler({
  getSSNState,
LAUNCH() {
  console.log("Launching bot....");
  console.log(app.config.handlers);
  //let session = this.$session.$data;
  //session.Channel = architect.getParameter(this.$request, "Channel");
  this.$session.$data = {};
  this.$session.$data.retryCount = 0;
  this.$session.$data.transferReason = "Max-NM-2250";
  this.$session.$data.disconnect = false;
  this.$session.$data.repeatReprompt = "";
  this.$session.$data.$lastSpeech = {};
  this.$session.$data.mainMenuReprompt = false;
  console.log("Session Data", this.$session.$data);
  this.followUpState("");
  return this.toIntent('MainMenu');
},

MainMenu(){
  console.log("************************* MainMenu ******************************");
  if(this.$session.$data.mainMenuReprompt){
    this.$speech.addText(this.t("2250_I_2"));
    this.ask(this.$speech);
    this.followUpState("reconfirm")
  } else {
    this.$speech.addText(this.t("2250_I_1"));
    this.ask(this.$speech);
  }
},

reconfirm: {
  YesIntent(){
    console.log("Reconfirm Yes Intent");
    this.$session.$data.mainMenuReprompt = false;
    this.toStateIntent("", "MainMenu");
    return;
  },
  NoIntent(){
    console.log("No Intent");
    this.$speech.addText(this.t("250_M_1"));
    this.$session.$data.transferReason = "RetryNoIntent";
    this.toStateIntent("", "ChatWrapup");
    return;
  }
},

Collect_SSN_DCN(){
  console.log("************************* SSN Input ******************************")
  return this.toStateIntent("getSSNState", "get_ssn_start");
},

ChatWrapup(){
  console.log("********************ChatWrapup - Connecting to Live Agent*******************************");
  this.$session.$data.sendToArchitect = {
    architectParam: {
      transferReason: this.$session.$data.transferReason,
      intentName: this.$session.$data.intentName,
      disconnect: this.$session.$data.disconnect //Flag
    }
  }
},
OfficeClosedIntent(){
  //300_I_1
  this.$session.$data.intentName = "OfficeClosedIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_1"));
  this.toIntent("ConfirmAnswer");
},
MOHealthNetEligibilitytIntent(){
  //300_I_2
  this.$session.$data.intentName = "MOHealthNetEligibilitytIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_2"));
  this.toIntent("ConfirmAnswer");
},
DocumentVerificationIntent(){
  //300_I_3
  this.$session.$data.intentName = "DocumentVerificationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_3"));
  this.toIntent("ConfirmAnswer");
},
FoodStampGeneralInfoIntent(){
  //300_I_4
  this.$session.$data.intentName = "FoodStampGeneralInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_4"));
  this.toIntent("ConfirmAnswer");
},
EBTCardIntent(){
  //300_I_5
  this.$session.$data.intentName = "EBTCardIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_5"));
  this.toIntent("ConfirmAnswer");
},
SnapBenefitsInfoIntent(){
  //300_I_6
  this.$session.$data.intentName = "SnapBenefitsInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_6"));
  this.toIntent("ConfirmAnswer");
},
PandemicEBTIntent(){
  //300_I_7
  this.$session.$data.intentName = "PandemicEBTIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_7"));
  this.toIntent("ConfirmAnswer");
},
TempAssistanceIntent(){
  //300_I_8
  this.$session.$data.intentName = "TempAssistanceIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_8"));
  this.toIntent("ConfirmAnswer");
},
MedicaidRenewalIntent(){
  //300_I_9
  this.$session.$data.intentName = "MedicaidRenewalIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_9"));
  this.toIntent("ConfirmAnswer");
},
FoodBankIntent(){
  //300_I_10
  this.$session.$data.intentName = "FoodBankIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_10"));
  this.toIntent("ConfirmAnswer");
},
ApplyForBenefitsIntent(){
  //300_I_11
  this.$session.$data.intentName = "ApplyForBenefitsIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_11"));
  this.toIntent("ConfirmAnswer");
},
EBTCardUsabilityIntent(){
  //300_I_12
  this.$session.$data.intentName = "EBTCardUsabilityIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_12"));
  this.toIntent("ConfirmAnswer");
},
EBTCardInformationIntent(){
  //300_I_13
  this.$session.$data.intentName = "EBTCardInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_13"));
  this.toIntent("ConfirmAnswer");
},
PhoneBenefitsIntent(){
  //300_I_14
  this.$session.$data.intentName = "PhoneBenefitsIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_14"));
  this.toIntent("ConfirmAnswer");
},
UtilityAssistanceIntent(){
  //300_I_15
  this.$session.$data.intentName = "UtilityAssistanceIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_15"));
  this.toIntent("ConfirmAnswer");
},
TABenefitsExtensionIntent(){
  //300_I_16
  this.$session.$data.intentName = "TABenefitsExtensionIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_16"));
  this.toIntent("ConfirmAnswer");
},
DrugTestingIntent(){
  //300_I_17
  this.$session.$data.intentName = "DrugTestingIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_17"));
  this.toIntent("ConfirmAnswer");
},
ChildCareIntent(){
  //300_I_18
  this.$session.$data.intentName = "ChildCareIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_18"));
  this.toIntent("ConfirmAnswer");
},
BusinessEssentialsIntent(){
  //300_I_19
  this.$session.$data.intentName = "BusinessEssentialsIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_19"));
  this.toIntent("ConfirmAnswer");
},
WICServicesIntent(){
  //300_I_20
  this.$session.$data.intentName = "WICServicesIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_20"));
  this.toIntent("ConfirmAnswer");
},
CovidInformationIntent(){
  //300_I_21
  this.$session.$data.intentName = "CovidInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_21"));
  this.toIntent("ConfirmAnswer");
},
CovidMaskIntent(){
  //300_I_22
  this.$session.$data.intentName = "CovidMaskIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_22"));
  this.toIntent("ConfirmAnswer");
},
FoodStampsIssuesIntent(){
  //300_I_23
  this.$session.$data.intentName = "FoodStampsIssuesIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_23"));
  this.toIntent("ConfirmAnswer");
},
ShowMeHealthyBabiesIntent(){
  //300_I_24
  this.$session.$data.intentName = "ShowMeHealthyBabiesIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_24"));
  this.toIntent("ConfirmAnswer");
},
ChangeReportingIntent(){
  //300_I_25
  this.$session.$data.intentName = "ChangeReportingIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_25"));
  this.toIntent("ConfirmAnswer");
},
ThankYouIntent(){
  //300_I_27
  this.$session.$data.intentName = "ThankYouIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_27"));
  this.toIntent("ConfirmAnswer");
},
FoodStampEligibilityIntent(){
  //300_I_28
  this.$session.$data.intentName = "FoodStampEligibilityIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_28"));
  this.toIntent("ConfirmAnswer");
},
BenefitStartTimeIntent(){
  //300_I_29
  this.$session.$data.intentName = "BenefitStartTimeIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_29"));
  this.toIntent("ConfirmAnswer");
},
BeingChildCareProviderIntent(){
  //300_I_30
  this.$session.$data.intentName = "BeingChildCareProviderIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_30"));
  this.toIntent("ConfirmAnswer");
},
FoodStampInterviewIntent(){
  //300_I_31
  this.$session.$data.intentName = "FoodStampInterviewIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_31"));
  this.toIntent("ConfirmAnswer");
},
FoodStampBackDateIntent(){
  //300_I_32
  this.$session.$data.intentName = "FoodStampBackDateIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_32"));
  this.toIntent("ConfirmAnswer");
},
TransitionalMedicaidIntent(){
  //300_I_33
  this.$session.$data.intentName = "TransitionalMedicaidIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_33"));
  this.toIntent("ConfirmAnswer");
},
TempMedicaidIntent(){
  //300_I_34
  this.$session.$data.intentName = "TempMedicaidIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_34"));
  this.toIntent("ConfirmAnswer");
},
CovidTestedPositiveIntent(){
  //300_I_35
  this.$session.$data.intentName = "CovidTestedPositiveIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_35"));
  this.toIntent("ConfirmAnswer");
},
OnlineAccountIssueIntent(){
  //300_I_36
  this.$session.$data.intentName = "OnlineAccountIssueIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_36"));
  this.toIntent("ConfirmAnswer");
},
MedicaidHelpIntent(){
  //300_I_37
  this.$session.$data.intentName = "MedicaidHelpIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_37"));
  this.toIntent("ConfirmAnswer");
},
SpanishHelpIntent(){
  //300_I_38
  this.$session.$data.intentName = "SpanishHelpIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_38"));
  this.toIntent("ConfirmAnswer");
},
ReplaceMedicaidCardIntent(){
  //300_I_39
  this.$session.$data.intentName = "ReplaceMedicaidCardIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_39"));
  this.toIntent("ConfirmAnswer");
},
Form1095InformationIntent(){
  //300_I_40
  this.$session.$data.intentName = "Form1095InformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_40"));
  this.toIntent("ConfirmAnswer");
},
AgentInformationIntent(){
  //300_I_41
  this.$session.$data.intentName = "AgentInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_41"));
  this.toIntent("ConfirmAnswer");
},
ApplicationStatusIntent(){
  //300_I_42
  this.$session.$data.intentName = "ApplicationStatusIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_42"));
  this.toIntent("ConfirmAnswer");
},
TaxesInformationIntent(){
  //300_I_43
  this.$session.$data.intentName = "TaxesInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_43"));
  this.toIntent("ConfirmAnswer");
},
ChildAbuseInformationIntent(){
  //300_I_44
  this.$session.$data.intentName = "ChildAbuseInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_44"));
  this.toIntent("ConfirmAnswer");
},
EBTCashBackIntent(){
  //300_I_45
  this.$session.$data.intentName = "EBTCashBackIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_45"));
  this.toIntent("ConfirmAnswer");
},
AppealRightsIntent(){
  //300_I_46
  this.$session.$data.intentName = "AppealRightsIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_46"));
  this.toIntent("ConfirmAnswer");
},
UnemploymentIncomeInfoIntent(){
  //300_I_47
  this.$session.$data.intentName = "UnemploymentIncomeInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_47"));
  this.toIntent("ConfirmAnswer");
},
PowerOfAttorneyIntent(){
  //300_I_48
  this.$session.$data.intentName = "PowerOfAttorneyIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_48"));
  this.toIntent("ConfirmAnswer");
},
HIPPProgramInfoIntent(){
  //300_I_49
  this.$session.$data.intentName = "HIPPProgramInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_49"));
  this.toIntent("ConfirmAnswer");
},
FoodPackageIntent(){
  //300_I_50
  this.$session.$data.intentName = "FoodPackageIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_50"));
  this.toIntent("ConfirmAnswer");
},
FileComplaintIntent(){
  //300_I_51
  this.$session.$data.intentName = "FileComplaintIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_51"));
  this.toIntent("ConfirmAnswer");
},
AppealUnemploymentCompIntent(){
  //300_I_52
  this.$session.$data.intentName = "AppealUnemploymentCompIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_52"));
  this.toIntent("ConfirmAnswer");
},
QmbSlmbInformationIntent(){
  //300_I_53
  this.$session.$data.intentName = "QmbSlmbInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_53"));
  this.toIntent("ConfirmAnswer");
},
SSNCardInformationIntent(){
  //300_I_54
  this.$session.$data.intentName = "SSNCardInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_54"));
  this.toIntent("ConfirmAnswer");
},
MedicaidInformationIntent(){
  //300_I_55
  this.$session.$data.intentName = "MedicaidInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_55"));
  this.toIntent("ConfirmAnswer");
},
FeeForServiceProviderInfoIntent(){
  //300_I_56
  this.$session.$data.intentName = "FeeForServiceProviderInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_56"));
  this.toIntent("ConfirmAnswer");
},
UnemploymentCompInfoIntent(){
  //300_I_57
  this.$session.$data.intentName = "UnemploymentCompInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_57"));
  this.toIntent("ConfirmAnswer");
},
MedicaidCoverageInfoIntent(){
  //300_I_58
  this.$session.$data.intentName = "MedicaidCoverageInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_58"));
  this.toIntent("ConfirmAnswer");
},
FoodStampExtraBenefitsIntent(){
  //300_I_59
  this.$session.$data.intentName = "FoodStampExtraBenefitsIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_59"));
  this.toIntent("ConfirmAnswer");
},
DisabilityInformationIntent(){
  //300_I_60
  this.$session.$data.intentName = "DisabilityInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_60"));
  this.toIntent("ConfirmAnswer");
},
ReapplyForBenefitsIntent(){
  //300_I_61
  this.$session.$data.intentName = "ReapplyForBenefitsIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_61"));
  this.toIntent("ConfirmAnswer");
},
PregnancyNewBornBenefitsIntent(){
  //300_I_62
  this.$session.$data.intentName = "PregnancyNewBornBenefitsIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_62"));
  this.toIntent("ConfirmAnswer");
},
FelonyExemptionInfoIntent(){
  //300_I_63
  this.$session.$data.intentName = "FelonyExemptionInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_63"));
  this.toIntent("ConfirmAnswer");
},
InHomeServiceApplicationInfoIntent(){
  //300_I_64
  this.$session.$data.intentName = "InHomeServiceApplicationInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_64"));
  this.toIntent("ConfirmAnswer");
},
StimulusCheckInfoIntent(){
  //300_I_65
  this.$session.$data.intentName = "StimulusCheckInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_65"));
  this.toIntent("ConfirmAnswer");
},
HealthPlanChangeInfoIntent(){
  //300_I_66
  this.$session.$data.intentName = "HealthPlanChangeInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_66"));
  this.toIntent("ConfirmAnswer");
},
TADiversionInfoIntent(){
  //300_I_67
  this.$session.$data.intentName = "TADiversionInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_67"));
  this.toIntent("ConfirmAnswer");
},
FoodBucksProgramInfoIntent(){
  //300_I_68
  this.$session.$data.intentName = "FoodBucksProgramInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_68"));
  this.toIntent("ConfirmAnswer");
},
SpendDownInformationIntent(){
  //300_I_69
  this.$session.$data.intentName = "SpendDownInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_69"));
  this.toIntent("ConfirmAnswer");
},
SkillUpProgramInfoIntent(){
  //300_I_70
  this.$session.$data.intentName = "SkillUpProgramInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_70"));
  this.toIntent("ConfirmAnswer");
},
VitalRecordsInfoIntent(){
  //300_I_71
  this.$session.$data.intentName = "VitalRecordsInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_71"));
  this.toIntent("ConfirmAnswer");
},
CloseCaseInformationIntent(){
  //300_I_72
  this.$session.$data.intentName = "CloseCaseInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_72"));
  this.toIntent("ConfirmAnswer");
},
SSDQualifificationInfoIntent(){
  //300_I_73
  this.$session.$data.intentName = "SSDQualifificationInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_73"));
  this.toIntent("ConfirmAnswer");
},
FamilyCoverageInfoIntent(){
  //300_I_74
  this.$session.$data.intentName = "FamilyCoverageInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_74"));
  this.toIntent("ConfirmAnswer");
},
MedicaidCardProcessDurationIntent(){
  //300_I_75
  this.$session.$data.intentName = "MedicaidCardProcessDurationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_75"));
  this.toIntent("ConfirmAnswer");
},
ParentsGuardiansInfoIntent(){
  //300_I_76
  this.$session.$data.intentName = "ParentsGuardiansInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_76"));
  this.toIntent("ConfirmAnswer");
},
ChildSupportInfoIntent(){
  //300_I_77
  this.$session.$data.intentName = "ChildSupportInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_77"));
  this.toIntent("ConfirmAnswer");
},
FoodStampUsageOnlineIntent(){
  //300_I_78
  this.$session.$data.intentName = "FoodStampUsageOnlineIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_78"));
  this.toIntent("ConfirmAnswer");
},
MonthlyBenefitsInfoIntent(){
  //300_I_79
  this.$session.$data.intentName = "MonthlyBenefitsInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_79"));
  this.toIntent("ConfirmAnswer");
},
MedicaidMedicareInfoIntent(){
  //300_I_80
  this.$session.$data.intentName = "MedicaidMedicareInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_80"));
  this.toIntent("ConfirmAnswer");
},
ExtraInsuranceInfoIntent(){
  //300_I_81
  this.$session.$data.intentName = "ExtraInsuranceInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_81"));
  this.toIntent("ConfirmAnswer");
},
CopayInformationIntent(){
  //300_I_82
  this.$session.$data.intentName = "CopayInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_82"));
  this.toIntent("ConfirmAnswer");
},
ChildCareRatesIntent(){
  //300_I_83
  this.$session.$data.intentName = "ChildCareRatesIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_83"));
  this.toIntent("ConfirmAnswer");
},
CoverageInformationIntent(){
  //300_I_84
  this.$session.$data.intentName = "CoverageInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_84"));
  this.toIntent("ConfirmAnswer");
},
LowIncomeFamilyBenefitIntent(){
  //300_I_85
  this.$session.$data.intentName = "LowIncomeFamilyBenefitIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_85"));
  this.toIntent("ConfirmAnswer");
},
PandemicEBTProblemIntent(){
  //300_I_86
  this.$session.$data.intentName = "PandemicEBTProblemIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_86"));
  this.toIntent("ConfirmAnswer");
},
PandemicEBTChangeInquiryIntent(){
  //300_I_87
  this.$session.$data.intentName = "PandemicEBTChangeInquiryIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_87"));
  this.toIntent("ConfirmAnswer");
},
UnearnedIncomeIntent(){
  //300_I_88
  this.$session.$data.intentName = "UnearnedIncomeIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_88"));
  this.toIntent("ConfirmAnswer");
},
EarnedIncomeIntent(){
  //300_I_89
  this.$session.$data.intentName = "EarnedIncomeIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_89"));
  this.toIntent("ConfirmAnswer");
},
HeadOfHouseholdIntent(){
  //300_I_90
  this.$session.$data.intentName = "HeadOfHouseholdIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_90"));
  this.toIntent("ConfirmAnswer");
},
ProofOfIdentityInfoIntent(){
  //300_I_91
  this.$session.$data.intentName = "ProofOfIdentityInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_91"));
  this.toIntent("ConfirmAnswer");
},
ProofOfResidenceInfoIntent(){
  //300_I_92
  this.$session.$data.intentName = "ProofOfResidenceInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_92"));
  this.toIntent("ConfirmAnswer");
},
UpdatingHouseholdIncomeIntent(){
  //300_I_93
  this.$session.$data.intentName = "UpdatingHouseholdIncomeIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_93"));
  this.toIntent("ConfirmAnswer");
},
OnlineApplicationProblemIntent(){
  //300_I_94
  this.$session.$data.intentName = "OnlineApplicationProblemIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_94"));
  this.toIntent("ConfirmAnswer");
},
OnlineApplicationCorrectionIntent(){
  //300_I_95
  this.$session.$data.intentName = "OnlineApplicationCorrectionIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_95"));
  this.toIntent("ConfirmAnswer");
},
ChangeWebsiteLanguageIntent(){
  //300_I_96
  this.$session.$data.intentName = "ChangeWebsiteLanguageIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_96"));
  this.toIntent("ConfirmAnswer");
},
ApplicationMoreThanTenDaysIntent(){
  //300_I_97
  this.$session.$data.intentName = "ApplicationMoreThanTenDaysIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_97"));
  this.toIntent("ConfirmAnswer");
},
TANFinformationIntent(){
  //300_I_98
  this.$session.$data.intentName = "TANFinformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_98"));
  this.toIntent("ConfirmAnswer");
},
BecomingEBTRetailerIntent(){
  //300_I_99
  this.$session.$data.intentName = "BecomingEBTRetailerIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_99"));
  this.toIntent("ConfirmAnswer");
},
NursingHomeInfoIntent(){
  //300_I_100
  this.$session.$data.intentName = "NursingHomeInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_100"));
  this.toIntent("ConfirmAnswer");
},
UtilitiesInformationIntent(){
  //300_I_101
  this.$session.$data.intentName = "UtilitiesInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_101"));
  this.toIntent("ConfirmAnswer");
},
UnsubscribeEmailInfoIntent(){
  //300_I_102
  this.$session.$data.intentName = "UnsubscribeEmailInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_102"));
  this.toIntent("ConfirmAnswer");
},
RegisterForJobsIntent(){
  //300_I_103
  this.$session.$data.intentName = "RegisterForJobsIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_103"));
  this.toIntent("ConfirmAnswer");
},
DirectDepositInfoIntent(){
  //300_I_104
  this.$session.$data.intentName = "DirectDepositInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_104"));
  this.toIntent("ConfirmAnswer");
},
MWAInformationIntent(){
  //300_I_105
  this.$session.$data.intentName = "MWAInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_105"));
  this.toIntent("ConfirmAnswer");
},
ChildCustodyInformationIntent(){
  //300_I_106
  this.$session.$data.intentName = "ChildCustodyInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_106"));
  this.toIntent("ConfirmAnswer");
},
PresumptiveEligibilityIntent(){
  //300_I_107
  this.$session.$data.intentName = "PresumptiveEligibilityIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_107"));
  this.toIntent("ConfirmAnswer");
},
MidCertInformationIntent(){
  //300_I_108
  this.$session.$data.intentName = "MidCertInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_108"));
  this.toIntent("ConfirmAnswer");
},
MIDCertAdditionalInfoIntent(){
  //300_I_109
  this.$session.$data.intentName = "MIDCertAdditionalInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_109"));
  this.toIntent("ConfirmAnswer");
},
CHIPInformationIntent(){
  //300_I_110
  this.$session.$data.intentName = "CHIPInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_110"));
  this.toIntent("ConfirmAnswer");
},
PrimaryCareProviderInfoIntent(){
  //300_I_111
  this.$session.$data.intentName = "PrimaryCareProviderInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_111"));
  this.toIntent("ConfirmAnswer");
},
RegistrationPacketInfoIntent(){
  //300_I_112
  this.$session.$data.intentName = "RegistrationPacketInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_112"));
  this.toIntent("ConfirmAnswer");
},
NonEmergencyMedicalInfoIntent(){
  //300_I_113
  this.$session.$data.intentName = "NonEmergencyMedicalInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_113"));
  this.toIntent("ConfirmAnswer");
},
PremiumPaymentsInformationIntent(){
  //300_I_114
  this.$session.$data.intentName = "PremiumPaymentsInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_114"));
  this.toIntent("ConfirmAnswer");
},
BenefitEligibilityRequirementsIntent(){
  //300_I_115
  this.$session.$data.intentName = "BenefitEligibilityRequirementsIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_115"));
  this.toIntent("ConfirmAnswer");
},
MedicaidExpansionInfoIntent(){
  //300_I_116
  this.$session.$data.intentName = "MedicaidExpansionInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_116"));
  this.toIntent("ConfirmAnswer");
},
PhoneContactIssueIntent(){
  //300_I_117
  this.$session.$data.intentName = "PhoneContactIssueIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_117"));
  this.toIntent("ConfirmAnswer");
},
FraudReportingInformationIntent(){
  //300_I_118
  this.$session.$data.intentName = "FraudReportingInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_118"));
  this.toIntent("ConfirmAnswer");
},
BillingInformationIntent(){
  //300_I_119
  this.$session.$data.intentName = "BillingInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_119"));
  this.toIntent("ConfirmAnswer");
},
ApplicationFormReceiptIntent(){
  //300_I_120
  this.$session.$data.intentName = "ApplicationFormReceiptIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_120"));
  this.toIntent("ConfirmAnswer");
},
DeathReportingInformationIntent(){
  //300_I_121
  this.$session.$data.intentName = "DeathReportingInformationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_121"));
  this.toIntent("ConfirmAnswer");
},
ChildCareProviderRegistrationIntent(){
  //300_I_122
  this.$session.$data.intentName = "ChildCareProviderRegistrationIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_122"));
  this.toIntent("ConfirmAnswer");
},
ChildCareProviderTrainingIntent(){
  //300_I_123
  this.$session.$data.intentName = "ChildCareProviderTrainingIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_123"));
  this.toIntent("ConfirmAnswer");
},
TrackingCCPTrainingIntent(){
  //300_I_124
  this.$session.$data.intentName = "TrackingCCPTrainingIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_124"));
  this.toIntent("ConfirmAnswer");
},
PRRProcessingTimeIntent(){
  //300_I_125
  this.$session.$data.intentName = "PRRProcessingTimeIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_125"));
  this.toIntent("ConfirmAnswer");
},
FingerPrintingInfoIntent(){
  //300_I_126
  this.$session.$data.intentName = "FingerPrintingInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_126"));
  this.toIntent("ConfirmAnswer");
},
FingerPrintingWaiverInfoIntent(){
  //300_I_127
  this.$session.$data.intentName = "FingerPrintingWaiverInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_127"));
  this.toIntent("ConfirmAnswer");
},
CCPInquiryForEligibleChildIntent(){
  //300_I_128
  this.$session.$data.intentName = "CCPInquiryForEligibleChildIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_128"));
  this.toIntent("ConfirmAnswer");
},
InternetBenefitProgramInfoIntent(){
  //300_I_129
  this.$session.$data.intentName = "InternetBenefitProgramInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_129"));
  this.toIntent("ConfirmAnswer");
},
ChildPEBTBenefitsInfoIntent(){
  //300_I_130
  this.$session.$data.intentName = "ChildPEBTBenefitsInfoIntent";
  console.log("Intent Name: ", this.$session.$data.intentName);
  this.$speech.addText(this.t("300_I_130"));
  this.toIntent("ConfirmAnswer");
},
ConfirmAnswer(){
  console.log("++++++++++ Confirm Response +++++++++++++++++++++");
  this.$session.$data.transferReason = "Max-NM-400";
  this.$speech.addText(this.t("\n\n"));
  this.$speech.addText(this.t("400_I_1"));
  this.$session.$data.repeatReprompt = this.t("400_I_1");
  this.ask(this.$speech);
  this.followUpState("confirm")
},
confirm: {
    YesIntent(){
      console.log("Yes Intent");
      this.$session.$data.mainMenuReprompt = true;
      this.toStateIntent("", "MainMenu");
      return;
    },
    NoIntent(){
      console.log("No Intent");
      this.$speech.addText(this.t("250_M_1"));
      //this.$speech.addText("Going back to Architect... for transfer");
      //this.$session.$data.transferMsg = this.$speech;
      //this.tell(this.$speech);
      this.$session.$data.transferReason = "NoIntent";
      this.toStateIntent("", "ChatWrapup");
      return;
    }
  },
Unhandled(){
    console.log("Main Unhandled");
  //increment retryCount
    this.$session.$data.retryCount++;
  //Max Retries
    if (Utils.isNoMatch(this, app)){
      let retriesCount = this.$session.$data.$retries || 0;
      this.$session.$data.transferReason = this.$session.$data.transferReason ? this.$session.$data.transferReason : "Max-NM-2250";
      console.log("State and intent from Jovo:" + this.getState());
      console.log(this.$reprompt);
      console.log("******************************* Repeat Reprompt: ", this.$session.$data.repeatReprompt);
      let initialPrompt = this.$session.$data.repeatReprompt || this.$session.$data.$lastSpeech;
      console.log("Initial prompt: " + initialPrompt);
      console.log("session: ");
      console.log(this.$session);
      if (retriesCount == 0) {
          console.log("**** In defaultNoMatch: first No-match");
          let speech = this.$speech;
          speech.addText(this.t("NM_1"));
          speech.addText(initialPrompt);
          this.followUpState(this.$session.$data.$state);
          this.ask(speech);

      } else if (retriesCount == 1) {
          console.log("**** In defaultNoMatch: second No-match");
          let speech = this.$speech;
          speech.addText(this.t("NM_2"));
          speech.addText(initialPrompt);
          this.followUpState(this.$session.$data.$state);
          this.ask(speech);

      } else {
          console.log("**** In defaultNoMatch: Max No-match");
          this.$speech.addText(this.t("250_M_1"));
          this.toStateIntent("", "ChatWrapup");
      }
    }
    if(Utils.isNoInput(this, app)){
      let noinputsCount = this.$session.$data.$noinputs || 0;
      console.log("\n**** In defaultNoInput: state and intent from Jovo:" + this.getState());
      console.log("**** In defaultNoInput: state to return to:" + this.$session.$data.$state);
      let initialPrompt = this.$session.$data.repeatReprompt || this.$session.$data.$lastSpeech;
      console.log("**** In defaultNoInput: Initial prompt to reprompt with: " + initialPrompt);
      //console.log("session: " );
      //console.log(this.$session);
      if (noinputsCount == 0) {
          console.log("**** In defaultNoInput: first noinput");
          let speech = this.$speech;
          speech.addText(this.t("NI_1"));
          speech.addText(initialPrompt);
          //console.log("**** In defaultNoInput: first noinput; played the reprompt, followup state is " + this.$session.$data.$state);
          this.followUpState(this.$session.$data.$state);
          this.ask(speech);

      } else if (noinputsCount == 1) {
          let speech = this.$speech;
          console.log("**** In defaultNoInput: second noinput");
          speech.addText(this.t("NI_2"));
          speech.addText(initialPrompt);
          //console.log("**** In defaultNoInput: second noinput; played the reprompt, followup state is " + this.$session.$data.$state);
          this.followUpState(this.$session.$data.$state);
          this.ask(speech);

      } else {
          console.log("**** In defaultNoInput: MaxNoInputs");
          this.$session.$data.disconnect = true;
          this.toStateIntent("", "ChatWrapup");
      }
    }
  }
});

module.exports = { app };
